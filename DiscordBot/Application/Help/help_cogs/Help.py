import discord
from discord.ext import commands

from DiscordBot import settings
from DiscordBot.Application.utils import checks

dev_help = {
    'help': '[music] [games] [admin] [dev]',
}

admin_help = {
    'help': '[music] [games] [admin]',
}

main_help = {
    'help': '[music] [games]',
}

music_commands = {
    'play': '[URL]',
    'add': '[URL]',
    'now_playing': 'Display currently playing song.',
    'q': 'Shows songs in the queue.',
    'skip': 'Skips current song.',
    'volume': '[1-100]',
    'stop': 'Stop currently playing song, and clears the queue.',
    'pause': 'Pauses currently playing song.',
    'resume': 'Resumes currently playing song.',
}

game_commands = {
    'roll': '[Positive number] Displays a random number between 1 - [your number]',
    'dice': '[1-6] Try to guess dice number.',
    'gulag': 'Picks a player at random from a list of players. Use "@[player], @[player]"',
}

admin_commands = {
    'None': 'More coming...',
}

dev_commands = {
    'purge': '[1 - 100]',
}


class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @checks.no_pm()
    @commands.command(name='help', description='Shows a list of commands.')
    async def help_(self, ctx, *, cat=''):
        await ctx.message.delete()
        command = cat.lower()
        if cat == '':
            if checks.is_dev():
                await self.dev_help(ctx)
            elif checks.is_admin():
                await self.dev_help(ctx)
            else:
                await self.reg_help(ctx)

        if command == 'music':
            await self.music(ctx)
        elif command == 'games':
            await self.games(ctx)
        elif command == 'dev' and checks.is_dev():
            await self.dev(ctx)
        elif command == 'admin' and checks.is_admin():
            await self.admin(ctx)

    @staticmethod
    async def music(ctx):
        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name='Music Help')

        for key, value in music_commands.items():
            embed.add_field(name=f"!{key}", value=value, inline=False)

        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @staticmethod
    async def games(ctx):
        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name='Game Help')

        for key, value in game_commands.items():
            embed.add_field(name=f"!{key}", value=value, inline=False)

        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @staticmethod
    async def dev(ctx):
        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name='Game Help')

        for key, value in dev_commands.items():
            embed.add_field(name=f"!{key}", value=value, inline=False)

        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @staticmethod
    async def admin(ctx):
        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name='Game Help')

        for key, value in admin_commands.items():
            embed.add_field(name=f"!{key}", value=value, inline=False)

        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @staticmethod
    async def reg_help(ctx):
        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name='Help')

        for key, value in main_help.items():
            embed.add_field(name=f"!{key}", value=value, inline=False)

        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @staticmethod
    async def admin_help(ctx):
        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name='Admin Help')

        for key, value in admin_help.items():
            embed.add_field(name=f"!{key}", value=value, inline=False)

        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @staticmethod
    async def dev_help(ctx):
        embed = discord.Embed(
            colour=discord.Colour.green()
        )

        embed.set_author(name='Dev Help')

        for key, value in dev_help.items():
            embed.add_field(name=f"!{key}", value=value, inline=False)

        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

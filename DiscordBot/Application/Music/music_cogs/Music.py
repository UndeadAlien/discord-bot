import shutil
from datetime import datetime
from os import path

import discord
from discord.ext import commands

from DiscordBot import settings
from DiscordBot.Application.Music.sources.youtube.YoutubeMusicPlayer import YoutubeMusicPlayer
from DiscordBot.Application.Music.sources.youtube.youtube import YTDLSource
from DiscordBot.Application.utils import checks


class Music(commands.Cog):
    __slots__ = ('bot', 'players')

    def __init__(self, bot):
        self.bot = bot
        self.players = {}

    async def cleanup(self, guild):
        try:
            await guild.voice_client.disconnect()
        except AttributeError:
            pass

        try:
            for entry in self.players[guild.id].queue._queue:
                if isinstance(entry, YTDLSource):
                    entry.cleanup()
            self.players[guild.id].queue._queue.clear()
        except KeyError:
            pass

        try:
            del self.players[guild.id]
        except KeyError:
            pass

    def get_player(self, ctx):
        try:
            player = self.players[ctx.guild.id]
        except KeyError:
            player = YoutubeMusicPlayer(ctx)
            self.players[ctx.guild.id] = player

        return player

    @checks.is_dev()
    @commands.command(name='join')
    async def join_(self, ctx):
        await ctx.message.delete()
        vc = ctx.author.voice.channel

        if vc:
            await vc.connect()

    @checks.is_dev()
    @commands.command()
    async def connect_(self, ctx, *, channel: discord.VoiceChannel):
        await ctx.message.delete()
        """Joins a voice channel"""

        if ctx.voice_client is not None:
            return await ctx.voice_client.move_to(channel)

        await channel.connect()

    @commands.command(name='play', aliases=['start', 'yt', 'music'])
    async def play_(self, ctx, *, url: str):
        await ctx.message.delete()
        await ctx.trigger_typing()

        vc = ctx.voice_client

        if not vc:
            await ctx.invoke(self.connect_)

        player = self.get_player(ctx)

        source = await YTDLSource.create_source(ctx, url, loop=self.bot.loop, download=True)

        await player.queue.put(source)

    @commands.command(name='add')
    async def add_song(self, ctx, *, url: str):
        await ctx.message.delete()
        vc = ctx.voice_client
        player = self.get_player(ctx)
        source = await YTDLSource.create_source(ctx, url, loop=None, download=True)

        player.playlist.append(f"{source.title}—{source.requester}")
        await player.queue.put(source)

    @commands.command(name='now_playing', aliases=['np', 'current', 'currentsong', 'playing'])
    async def now_playing_(self, ctx):
        await ctx.message.delete()
        """Display information about the currently playing song."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('I am not currently connected to voice!', delete_after=settings.BURN_MESSAGE_TIME)

        player = self.get_player(ctx)
        if not player.current:
            return await ctx.send('I am not currently playing anything!', delete_after=settings.BURN_MESSAGE_TIME)

        try:
            # Remove our previous now_playing message.
            await player.np.delete()
        except discord.HTTPException:
            pass

        embed = discord.Embed(
            title=f"Now Playing: {vc.source.title}",
            author=f"Requested by: {vc.source.requester}",
        )
        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @commands.command(name='queue', aliases=['q', 'playlist'])
    async def queue_info(self, ctx):
        await ctx.message.delete()
        """Retrieve a basic queue of upcoming songs."""
        vc = ctx.voice_client

        if not vc or not vc.is_connected():
            return await ctx.send('I am not currently connected to voice!', delete_after=settings.BURN_MESSAGE_TIME)

        player = self.get_player(ctx)

        for item in player.playlist:
            full_name = item.split("—")
            name = full_name[0]
            requester = full_name[1]
            embed = discord.Embed(title=f"{name}", description=f"{requester}", timestamp=datetime.now())
            await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

    @commands.command(name='skip', aliases=['next'])
    async def skip_(self, ctx):
        await ctx.message.delete()
        vc = ctx.voice_client
        if not vc or not vc.is_connected:
            return await ctx.send("Not currently playing anything!", delete_after=settings.BURN_MESSAGE_TIME)
        if vc.is_paused():
            pass
        elif not vc.is_playing():
            return

        vc.stop()
        await ctx.send(f"{ctx.author} skipped the song!", delete_after=settings.BURN_MESSAGE_TIME)

    @commands.command(name="volume", aliases=['vol'])
    async def volume_(self, ctx, volume: int):
        await ctx.message.delete()
        """Changes the player's volume"""

        if ctx.voice_client is None:
            return await ctx.send("Not connected to a voice channel.", delete_after=settings.BURN_MESSAGE_TIME)

        ctx.voice_client.source.volume = volume / 100
        await ctx.send("Changed volume to {}%".format(volume), delete_after=settings.BURN_MESSAGE_TIME)

    @commands.command(name='stop', aliases=['leave'])
    async def stop_(self, ctx):
        await ctx.message.delete()
        """Stops and disconnects the bot from voice"""

        try:
            await ctx.voice_client.disconnect()

            if path.exists('downloads'):
                shutil.rmtree('downloads')
        except AttributeError:
            pass

        try:
            del self.players[ctx.guild.id]
        except KeyError:
            pass

    @commands.command(name='pause')
    async def pause_(self, ctx):
        await ctx.message.delete()
        vc = ctx.voice_client
        if vc:
            if vc.is_connected and vc.is_playing():
                vc.pause()
                await ctx.send("Pausing Music...", delete_after=settings.BURN_MESSAGE_TIME)
        else:
            await ctx.send("Not in a voice channel.", delete_after=settings.BURN_MESSAGE_TIME)

    @commands.command(name='resume')
    async def resume_(self, ctx):
        await ctx.message.delete()
        vc = ctx.voice_client
        if vc:
            if vc.is_connected and vc.is_paused():
                vc.resume()
                await ctx.send("Resuming Music...", delete_after=settings.BURN_MESSAGE_TIME)
        else:
            await ctx.send("Not in a voice channel.", delete_after=settings.BURN_MESSAGE_TIME)

    @play_.before_invoke
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
            else:
                await ctx.send("You are not connected to a voice channel.")
                raise commands.CommandError("Author not connected to a voice channel.")
        elif ctx.voice_client.is_playing():
            ctx.voice_client.stop()

import asyncio

import discord
from async_timeout import timeout
from discord.ext import commands

from DiscordBot import settings

TIMEOUT = 60


class YoutubeMusicPlayer(commands.Cog):
    __slots__ = ('bot', '_guild', '_channel', '_cog', 'queue', 'next', 'current', 'np', 'volume')

    def __init__(self, ctx):
        self._cog = ctx.cog
        self.bot = ctx.bot
        self._guild = ctx.guild
        self._channel = ctx.channel

        self.queue = asyncio.Queue()
        self.next = asyncio.Event()

        self.playlist = []

        self.np = None
        self.volume = .15
        self.current = None

        ctx.bot.loop.create_task(self.player_loop())

    async def player_loop(self):
        """Our main player loop."""
        await self.bot.wait_until_ready()

        while not self.bot.is_closed():
            self.next.clear()

            try:
                # Wait for the next song. If we timeout cancel the player and disconnect...
                async with timeout(TIMEOUT):  # 5 minutes...
                    source = await self.queue.get()
            except asyncio.TimeoutError:
                return self.destroy(self._guild)

            self.current = source
            embed = discord.Embed(
                title=f"Now Playing: {source.title}",
                author=f"Requested by: {source.requester}",
            )
            source.volume = self.volume
            self._guild.voice_client.play(source, after=lambda _: self.bot.loop.call_soon_threadsafe(self.next.set))
            self.np = await self._channel.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

            await self.next.wait()

            # Make sure the FFmpeg process is cleaned up.
            source.cleanup()
            self.current = None

            try:
                # We are no longer playing this song...
                await self.np.delete()
            except discord.HTTPException:
                await self._channel.send("No songs left in the queue.", delete_after=settings.BURN_MESSAGE_TIME)
                # self._guild.voice_client.move_to('afk')
                pass

    def get_playlist(self):
        return self.playlist

    def destroy(self, guild):
        """Disconnect and cleanup the player."""
        return self.bot.loop.create_task(self._cog.cleanup(guild))

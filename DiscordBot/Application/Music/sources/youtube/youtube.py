import asyncio
import datetime as dt
from functools import partial
import discord
from youtube_dl import YoutubeDL

from DiscordBot import settings

ytdlopts = {
    'format': 'bestaudio/best',
    'outtmpl': 'downloads/%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'  # ipv6 addresses cause issues sometimes
}

ffmpegopts = {
    'before_options': '-nostdin',
    'options': '-vn'
}

ytdl = YoutubeDL(ytdlopts)


# Clear cache on start
# 404 Forbidden Error
# Usually songs that apart of a playlist
def clear_cache():
    with YoutubeDL(ytdlopts) as ydl:
        ydl.cache.remove()


class YTDLSource(discord.PCMVolumeTransformer):

    def __init__(self, source, *, data, requester):
        super().__init__(source)

        self.requester = requester

        self.title = data.get('title')
        self.web_url = data.get('webpage_url')
        self.duration = dt.timedelta(seconds=data['duration'])

    #
    # ***MODIFIED***
    #
    @classmethod
    async def create_source(cls, ctx, search: str, *, loop, download=False):

        loop = loop or asyncio.get_event_loop()

        to_run = partial(ytdl.extract_info, url=search, download=download)
        data = await loop.run_in_executor(None, to_run)

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        # Get information about the video with [DATA]

        embed = discord.Embed(
            title=f"Added {data['title']} to the Queue",
            description=f"Song Length: {dt.timedelta(seconds=data['duration'])}",
        )
        await ctx.send(embed=embed, delete_after=settings.BURN_MESSAGE_TIME)

        if download:
            source = ytdl.prepare_filename(data)
        else:
            return {'webpage_url': data['webpage_url'], 'requester': ctx.author, 'title': data['title']}

        return cls(discord.FFmpegPCMAudio(source), data=data, requester=ctx.author)


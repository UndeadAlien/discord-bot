from discord.ext import commands

from DiscordBot.Application.utils import checks


class Dev(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @checks.is_dev()
    @commands.command(name='purge')
    async def purge_(self, ctx, limit: int):
        if limit <= 50:
            await ctx.channel.purge(limit=limit)
        else:
            await ctx.send("Cannot purge more than 50 messages at once.")
from discord.ext import commands


def is_owner():
    async def predicate(ctx):
        return ctx.author.id == 123456789  # Enter Discord Personal ID

    return commands.check(predicate)


def is_dev():
    async def predicate(ctx):
        developers = [
            123456789,  # Enter Discord Personal ID
        ]
        return ctx.author.id in developers

    return commands.check(predicate)


def is_admin():
    async def predicate(ctx):
        admin = [
            123456789,  # Enter Discord Personal ID
        ]
        return ctx.author.id in admin

    return commands.check(predicate)


def no_pm():
    def predicate(ctx):
        if ctx.channel.name == "help":
            return True
        if ctx.guild is None:
            raise commands.NoPrivateMessage('This command cannot be used in PM.')
        return True

    return commands.check(predicate)


class ChannelError(commands.CommandError):
    def __init__(self, message):
        self.__message__ = message
        super().__init__(message)

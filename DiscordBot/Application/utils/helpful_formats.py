def format_money_commas(amount):
    return f'{amount:,.2f}'


def get_time(time):
    return time.strftime('%b-%d-%y %H:%M:%S')

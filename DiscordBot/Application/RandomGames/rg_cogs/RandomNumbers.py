from discord.ext import commands

from DiscordBot.Application.utils import checks

import random


class RandomNumbers(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @checks.no_pm()
    @commands.command(name='roll', description='Rolls between 1 and [Any positive number]')
    async def roll_(self, ctx, *, number: int):

        author = ctx.message.author

        roll_number = random.randrange(1, number)
        await ctx.send(f"{author.mention} {roll_number}")

    @checks.no_pm()
    @commands.command(name='dice', description='Rolls a dice. Enter your guess after dice.')
    async def dice_(self, ctx, *, guess: int):
        dice_number = random.randrange(1, 6)

        if guess == dice_number:
            await ctx.send("You win!")
        else:
            await ctx.send(f"Number was: {dice_number}")

    @checks.no_pm()
    @commands.command(name='gulag', description='Picks a player at random from a list of players.')
    async def gulag_(self, ctx, *args):
        winner = random.choice(args)

        await ctx.send(f"Winner is: {winner}")

    @checks.no_pm()
    @commands.command(name='blackjack', description='Play blackjack vs bot as the dealer')
    async def blackjack_(self, ctx):
        await ctx.send("Blackjack is coming soon.")

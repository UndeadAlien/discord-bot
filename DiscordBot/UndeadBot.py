import discord
import youtube_dl
from discord.ext import commands

from DiscordBot.Application.Dev.dev_cog.Dev import Dev
from DiscordBot.Application.Help.help_cogs.Help import Help
from DiscordBot.Application.Music.music_cogs.Music import Music
from DiscordBot.Application.RandomGames.rg_cogs.RandomNumbers import RandomNumbers
from DiscordBot.settings import BOT_TOKEN

from DiscordBot.Application.Music.sources.youtube import youtube

youtube_dl.utils.bug_reports_message = lambda: ''

DEBUG = False


class Bot(commands.AutoShardedBot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, game=discord.Game(name="!help for help"), **kwargs)

        self.remove_command('help')

        icogs = [
            # Music Cogs
            Music(self),
            Dev(self),
            RandomNumbers(self),
            Help(self),
        ]

        for cog in icogs:
            self.add_cog(cog)

    async def on_ready(self):
        print(f'Logged in as {bot.user}')
        print('------')
        youtube.clear_cache()
        await bot.change_presence(status=discord.Status.online, activity=discord.Game(name="!help for help"))


bot = Bot(
    command_prefix=commands.when_mentioned_or("!"),
    description='Discord Bot'
)

bot.run(BOT_TOKEN)
